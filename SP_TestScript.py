import requests
import json

def PrintFail(p_dictOfFailedTests):
	'''
	Prints out a list of what requests need to be fixed.
	'''
	t_counter = 0
	if (len(p_dictOfFailedTests) != 0):
		print("The following needs to be fixed: \n")
		for m_test in p_dictOfFailedTests:
			t_counter += 1	
			print(str(t_counter) + ". " + m_test + "\n")

def RecordError(p_testName, p_subTest, p_request, p_dictOfFailedTests):
	'''
	Add Request to the dictionary of failed tests.
	'''
	p_dictOfFailedTests[p_testName + " " + p_subTest] = p_request
	return p_dictOfFailedTests

def AccountRecorder(p_request):
	print(type(p_request))

def StringCombiner(p_string1, p_string2):
	return p_string1 + " " + p_string2


def RequestProcessing(p_request, p_expectedResponseHeader, p_expectedResponseBody, p_expectedStatusCode):
	'''
	Processes requests and displays test results.
	'''
	m_responseBody = p_request.text
	m_statusCode = p_request.status_code

	if type(p_expectedResponseBody) == str:
		if (m_responseBody == p_expectedResponseBody):
			if m_statusCode == p_expectedStatusCode:
				print("STATUS CODE: " + str(m_statusCode))
				print("RESPONSE BODY: " + m_responseBody + "\n")
				print("Test Result: SUCCESS \n")
				return True
			else:
				print("ERROR: Status Code is incorrect! \n")
				print("Expected Status Code: " + str(p_expectedStatusCode) + "\n")
				print("Actual Status Code: " + str(m_statusCode) + "\n")
				return False
		else:
			print("ERROR: Response body is incorrect! \n")
			print("Expected Response: " + p_expectedResponseBody + "\n")
			print("Actual Response: " + m_responseBody + "\n")

	elif type(p_expectedResponseBody) == list:

		for m_responseBodyKey in json.loads(m_responseBody):
			if m_responseBodyKey not in p_expectedResponseBody:
				print("ERROR: Response body is incorrect! \n")
				print("Expected Response Keys: " + str(p_expectedResponseBody) + "\n")
				print("Actual Response Keys: " + m_responseBody + "\n")
				print("Missing Key: " + m_responseBodyKey + "\n")
				return False
			else:
				if m_statusCode == p_expectedStatusCode:
					print("STATUS CODE: " + str(m_statusCode))
					print("RESPONSE BODY: " + m_responseBody + "\n")
					print("Test Result: SUCCESS \n")
					return True
				else:
					print("ERROR: Status Code is incorrect! \n")
					print("Expected Status Code: " + str(p_expectedStatusCode) + "\n")
					print("Actual Status Code: " + str(m_statusCode) + "\n")
					return False				

def SendPOSTRequest(p_baseURL, p_URLParameters, p_requestHeader, p_requestBody):
	'''
	Sends POST request.
	'''
	print(p_baseURL + p_URLParameters)
	p_request = requests.post(p_baseURL + p_URLParameters, p_requestBody, headers = p_requestHeader)
	return p_request


def SendGETRequest(p_baseURL, p_URLParameters, p_requestHeader, p_requestBody):
	'''
	Sends GET request.
	'''
	print(p_requestHeader)
	print(p_baseURL + p_URLParameters, p_requestBody)
	p_request = requests.get(p_baseURL + p_URLParameters, p_requestBody, headers=p_requestHeader)
	return p_request


def SendPUTRequest(p_baseURL, p_URLParameters, p_requestHeader, p_requestBody):
	'''
	Sends PUT request.
	'''
	p_request = requests.put(p_baseURL + p_URLParameters, p_requestBody, headers = p_requestHeader)
	return p_request

def SendPATCHRequest(p_baseURL, p_URLParameters, p_requestHeader, p_requestBody):
	'''
	Sends PATCH request.
	'''
	p_request = requests.patch(p_baseURL + p_URLParameters, p_requestBody, headers = p_requestHeader)
	return p_request

def SendDELRequest(p_baseURL, p_URLParameters, p_requestHeader, p_requestBody):
	'''
	Sends DEL request.
	'''
	p_request = requests.delete(p_baseURL + p_URLParameters, p_requestBody, headers = p_requestHeader)
	return p_request







def execute():
	'''
	Initiates Test Cases
	'''

	#Initializing Variables
	m_testCount = 0
	m_dictOfFailedTests = {}

	m_accountInfov1 = {} #Used to aquire tokenId
	m_accountInfov2 = {}

	#POST /user/register
	m_testName = "POST /user/register"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To create a parent/teacher account. Requires ‘ACTIVE’ CMS records with user role in ‘cms’ collection. User can register with the same email until the account status is ‘ACTIVE’. 

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/register"
	m_requestHeader = {}
	m_requestBody = {"firstName" : "user1" , "lastName" : "sample1", "email" : "user1@mailinator.com", "password" : "aaaaaaaa", "sp_news" : "true", "role" : "PARENT"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Thank you for registering with us. Soon you will get a confirmation email to verify your account"}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	


	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/register"
	m_requestHeader = {}
	m_requestBody = {"firstName" : "user1" , "lastName" : "sample1", "email" : "user1@mailinator.com", "password" : "aaaaaaaa", "sp_news" : "true", "role" : "TEACHER"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Thank you for registering with us. Soon you will get a confirmation email to verify your account"}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")

	'''

	*****************  Commented out because we are testing statusCode 200 first during phase 1 *************************


	m_subTest = "HTTP Status Code: 500"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/register"
	m_requestHeader = {}
	m_requestBody = {"firstName" : "user" , "lastName" : "sample", "email" : "user@mailinator.com", "password" : "aaaaaaaa", "sp_news" : "true", "role" : "PARENT"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Unable to get or set records from database collection."}'
	m_statusCode = 412
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)



	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)



	print("----------------------------------------------------------------------------------------------")


	m_subTest = "HTTP Status Code: 400"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/register"
	m_requestHeader = {}
	m_requestBody = {"firstName" : "user" , "lastName" : "sample", "email" : "user@mailinator.com", "password" : "aaaaaaaa", "sp_news" : "true", "role" : "PARENT"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Request body doesn’t have ‘password’ or ‘oldPassword’ values."}'
	m_statusCode = 412
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)



	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)



	print("----------------------------------------------------------------------------------------------")


	m_subTest = "HTTP Status Code: 400"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/register"
	m_requestHeader = {}
	m_requestBody = {"firstName" : "user" , "lastName" : "sample", "email" : "user@mailinator.com", "password" : "aaaaaaaa", "sp_news" : "true", "role" : "PARENT"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Email address is already taken"}'
	m_statusCode = 412
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)



	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)



	print("----------------------------------------------------------------------------------------------")


	m_subTest = "HTTP Status Code: 400"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/register"
	m_requestHeader = {}
	m_requestBody = {"firstName" : "user" , "lastName" : "sample", "email" : "user@mailinator.com", "password" : "aaaaaaaa", "sp_news" : "true", "role" : "PARENT"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Email address is already taken"}'
	m_statusCode = 412
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)



	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)



	print("----------------------------------------------------------------------------------------------")


	m_subTest = "HTTP Status Code: 412"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/register"
	m_requestHeader = {}
	m_requestBody = {"firstName" : "user" , "lastName" : "sample", "email" : "user@mailinator.com", "password" : "aaaaaaaa", "sp_news" : "true", "role" : "PARENT"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Email address is already taken"}'
	m_statusCode = 412
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)



	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)

	'''



	m_testName = "POST /:role/resendemail"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To send a verification email to the user based on the user role i.e., ‘PARENT’ or ‘TEACHER’.

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/resendemail"
	m_requestHeader = {}
	m_requestBody = {"email" : "victorpnoverification@mailinator.com"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Thank you for registering with us. Soon you will get a confirmation email to verify your account"}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/teacher/resendemail"
	m_requestHeader = {}
	m_requestBody = {"email" : "victorptnoverification@mailinator.com"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Thank you for registering with us. Soon you will get a confirmation email to verify your account"}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)


	print("----------------------------------------------------------------------------------------------")

	m_testName = "POST /user/v2/login"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To login into user account irrespective of the user’s role. Requires ‘ACTIVE’ CMS records with user role in ‘cms’ collection.
	#Req: token uniquely generated every time we login, need to figure out test case 

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/v2/login"
	m_requestHeader = {}
	m_requestBody = {"identifier" : "victorpt@mailinator.com", "password" : "12345678"}
	m_responseHeader = ""
	m_responseBody = ["owner", "firstName", "lastName", "email", "role", "welcomefeed", "terms", "privacy", "status", "username", "createdAt", "updatedAt", "confirmationToken", "playerLimit", "loginToken", "loginTokenExpires", "resetPasswordToken", "resetPasswordExpires", "customWordsLimit", "tokenId", "playerCount", "id", "bucketUrl", "settings"]
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	else:
		m_accountInfov2 = json.loads(m_request.text)
	


	print("----------------------------------------------------------------------------------------------")

	'''
	************************************ CONFIRMATION TOKEN IS "NONE"  ***********************************

	m_testName = "GET /user/confirmation/:token"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")
	
	#To change user status to 'ACTIVE' using confirmation token.
	#REQ: Confirmation Token is currently NULL, need to figure out how to generate.

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	print(m_accountInfo['confirmationToken'])
	m_URLParameters = "/user/confirmation/" + m_accountInfo['confirmationToken']
	m_requestHeader = {}
	m_requestBody = {}
	m_responseHeader = ""
	m_responseBody = '{"message":"success"}'
	m_statusCode = 200
	m_request = SendGETRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	
	
	print("----------------------------------------------------------------------------------------------")
	'''

	m_testName = "POST /v1/users/login (MG)"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#Sign-in a pre-registered user to the Square Panda system.

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/v1/users/login"
	m_requestHeader = {}
	m_requestBody = {"identifier" : "victorpt@mailinator.com", "password" : "12345678", "game":"5a0b9ea68863656c3cadcb9c"}
	m_responseHeader = ""
	m_responseBody = ["id","username", "firstName", "lastName", "terms", "privacy", "tokenId", "playerCount", "playerLimit", "role"]
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	else:
		m_accountInfov1 = json.loads(m_request.text)


	print("----------------------------------------------------------------------------------------------")


	'''

	*************************************** TESTS WORK, NEED TO MOVE TO BOTTOM OF TEST CASES ******************************************

	m_testName = "GET /user/:userId/logout"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To clear application session and remove user JWT token from ‘tokens’ collection.


	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/" + m_accountInfov2["id"] +"/logout"
	m_requestHeader = {'Authorization': 'Bearer ' + m_accountInfov2['tokenId']}
	m_requestBody = ""
	m_responseHeader = ""
	m_responseBody = ''
	m_statusCode = 200
	m_request = SendGETRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	

	print("----------------------------------------------------------------------------------------------")
	
	
	m_testName = "GET /v1/users/logout (MG)"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#Clear application session and remove user's JWT token from ‘tokens’ collection.  This is done for the user represented by the provided bearer token.

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/v1/users/logout"
	m_requestHeader = {'Authorization': 'Bearer ' + m_accountInfov1['tokenId']}
	m_requestBody = ""
	m_responseHeader = ""
	m_responseBody = ""
	m_statusCode = 200
	m_request = SendGETRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	

	print("----------------------------------------------------------------------------------------------")
	'''
	
	m_testName = "POST /v1/tokens (MG)"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#Generate a new Authorization token for a user using the previous Authorization token.

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/v1/tokens"
	m_requestHeader = {'Authorization': 'Bearer ' + m_accountInfov1['tokenId']}
	m_requestBody = {}
	m_responseHeader = ""
	m_responseBody = ["token"]
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	


	m_testName = "GET /user/globalvalues/:userId"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To get player limit, player count and custom words limit for a user.
	#Req: Need to handle unqieu token case

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/globalvalues/" + m_accountInfov2["id"]
	m_requestHeader = {'Authorization': 'Bearer ' + m_accountInfov2['tokenId']}
	m_responseHeader = ""
	m_responseBody = ["customWordsLimit", "playerLimit", "playerCount"]
	m_statusCode = 200
	m_request = SendGETRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	

	print("----------------------------------------------------------------------------------------------")
	
	
	m_testName = "GET /v1/users/:userId (MG)"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To get player limit, player count and custom words limit for a user.

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/v1/users/" + m_accountInfov1["id"]
	m_requestHeader = {'Authorization': 'Bearer ' + m_accountInfov1['tokenId']}
	m_requestBody = ""
	m_responseHeader = ""
	m_responseBody = ["customWordsLimit", "playerLimit", "playerCount"]
	m_statusCode = 200
	m_request = SendGETRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	

	
	print("----------------------------------------------------------------------------------------------")
	
	m_testName = "POST /games/v3/appUpdates"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To get website URL. Requires user Authorization token to get user details (role, player count and settings).
	#REQ: 

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/games/v3/appUpdates"
	m_requestHeader = {"Authorization" : m_accountInfov2['tokenId']}
	m_requestBody = {"user_id" : m_accountInfov2['id']}
	m_responseHeader = ""
	m_responseBody = ["role","childCount","updateVersionText","navigation_link","settings"]
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")

	
	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/games/v3/appUpdates"
	m_requestHeader = {"Authorization" : m_accountInfov2['tokenId']}
	m_requestBody = ""
	m_responseHeader = ""
	m_responseBody = ["updateVersionText","navigation_link"]
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")



	m_testName = "POST /:role/validate/email"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To validate user email using ‘user’ collection.


	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/validate/email"
	m_requestHeader = {}
	m_requestBody = {"email" : "victorp@mailinator.com"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Email already verified"}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/teacher/validate/email"
	m_requestHeader = {}
	m_requestBody = {"email" : "victorpt@mailinator.com"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Email already verified"}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	


	m_testName = "POST /portal/:role/validate/email"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To validate user email using ‘user’ collection for ‘PARENT’ and ‘TEACHER’ roles.


	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/portal/user/validate/email"
	m_requestHeader = {}
	m_requestBody = {"email" : "victorpnoverification@mailinator.com"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Email Acceptable"}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/portal/teacher/validate/email"
	m_requestHeader = {}
	m_requestBody = {"email" : "victorptnoverification@mailinator.com"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Email Acceptable"}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	



	m_testName = "POST /:role/forgot-password"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To send a forgot password email with a link to reset password.


	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/forgot-password"
	m_requestHeader = {}
	m_requestBody = {"email" : "victorp@mailinator.com"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Successfully sent new generated password to victorp@mailinator.com with further instructions."}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/teacher/forgot-password"
	m_requestHeader = {}
	m_requestBody = {"email" : "victorpt@mailinator.com"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Successfully sent new generated password to victorpt@mailinator.com with further instructions."}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	

	m_testName = "POST portal/:role/forgot-password"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To send a forgot password email with a link to reset password for users with ‘PARENT’ and ‘TEACHER’ roles.

	
	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/portal/user/forgot-password"
	m_requestHeader = {}
	m_requestBody = {"email" : "victorp@mailinator.com"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Successfully sent new generated password to victorp@mailinator.com with further instructions."}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	
	
	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/portal/teacher/forgot-password"
	m_requestHeader = {}
	m_requestBody = {"email" : "victorpt@mailinator.com"}
	m_responseHeader = ""
	m_responseBody = '{"message":"Successfully sent new generated password to victorpt@mailinator.com with further instructions."}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")

	'''
	************************************** resetPasswordToken returns None   ******************************************

	m_testName = "POST /:role/reset-password"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To reset the password using reset password token.

	
	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	print(m_accountInfov2)
	m_URLParameters = "/user/reset-password?token=" + m_accountInfov2["resetPasswordToken"]
	m_requestHeader = ""
	m_requestBody = {"password" : "12345678", "confirmPassword" : "12345678"}
	m_responseHeader = ""
	m_responseBody = '{"message" : "successfully reset your password"}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	
	
	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/teacher/reset-password?token=" +  m_accountInfov2["resetPasswordToken"]
	m_requestHeader = ""
	m_requestBody = {"password" : "12345678", "confirmPassword" : "12345678"}
	m_responseHeader = ""
	m_responseBody = '{"message" : "successfully reset your password"}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	
	'''
	
	'''
	*********************************** WORKS, NEED TO BE REORDERED *************************************************
	m_testName = "POST /user/:userId/change-password"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To change password.

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/"+ m_accountInfov2["id"]+"/change-password"
	m_requestHeader = {"Authorization" : "Bearer token"}
	m_requestBody = {"oldPassword" : "12345678", "password": "12345678", "confirmPassword" : "12345678"}
	m_responseHeader = ""
	m_responseBody = '{"message" : "successfully changed your password"}'
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	
	'''


	#######################################################  START HERE    #########################################


	'''
	m_testName = "PUT /v1/users/agreements/:category/:type (MG)"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To update user terms and privacy version. ID of the requester is derived from the given authentication token.

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/v1/users/agreemetns/cms/terms"
	m_requestHeader = {"Authorization" : "Bearer token"}
	m_requestBody = {"version" : "1.1"}
	m_responseHeader = ""
	m_responseBody = '{"message" : "Data updated successfully."}'
	m_statusCode = 200
	m_request = SendPUTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")



	m_testName = "PUT /user/:userId/edit-profile"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To update user details except email and role.

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/5abbe67fd05df9557fcf1673/edit-profile"
	m_requestHeader = {"Authorization" : "Bearer token"}
	m_requestBody = {"firstName":"user", "lastName":"sample"}
	m_responseHeader = ""
	m_responseBody = '{"message" : "Data updated successfully."}'
	m_statusCode = 200
	m_request = SendPUTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")




	m_testName = "GET /user/verify/token/:token"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")
	
	#To check whether the reset password token is active.
	#REQ: Need to use token from /user/v2/login/

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/user/confirmation/ed2954411376c87b0292f2585124d749bea5e74b"
	m_requestHeader = {}
	m_requestBody = {}
	m_responseHeader = ""
	m_responseBody = '{"message":"success"}'
	m_statusCode = 200
	m_request = SendGETRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	

	print("----------------------------------------------------------------------------------------------")
	'''

	m_testName = "POST /portal/:role/login"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To login the account with user role i.e., ‘PARENT’ or ‘TEACHER’. Requires ‘ACTIVE’ CMS records with user’s role in ‘cms’ collection.
	#REQ: Players resoonse body need to be expanded to test

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/portal/user/login"
	m_requestHeader = {}
	m_requestBody = {"identifier" : "victorp@mailinator.com", "password": "aaaaaaaa"}
	m_responseHeader = ""
	m_responseBody = ["owner", "firstName","lastName","email","role","welcomefeed","terms","privacy","status","username","createdAt","updatedAt","confirmationToken","confirmationTokenExpires","playerLimit","loginToken","loginTokenExpires","resetPasswordToken","resetPasswordExpires","customWordsLimit","tokenId","playerCount","id","player"]
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")
	
	'''

	m_testName = "POST /admin/signin"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To login into user account with "ADMIN", "SUPPORT", "MARKETING" roles.
	#REQ: Need an admin account

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/admin/signin"
	m_requestHeader = {}
	m_requestBody = {"identifier" : "mtuitydev@mailinator.com", "password": "mtuitydev"}
	m_responseHeader = ""
	m_responseBody = ["owner", "gender", "firstName","lastName","email","role", "menu", "terms","privacy","status","username","createdAt","updatedAt", "id","tokenId"]
	m_statusCode = 200
	m_request = SendPOSTRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")


	m_testName = "GET /admin/:userId/caller"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To login the account with user role i.e., ‘PARENT’ or ‘TEACHER’. Requires ‘ACTIVE’ CMS records with user’s role in ‘cms’ collection.
	#REQ: Players resoonse body need to be expanded to test

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/admin/signin"
	m_requestHeader = {}
	m_requestBody = {"identifier" : "mtuitydev@mailinator.com", "password": "mtuitydev"}
	m_responseHeader = ""
	m_responseBody = ["owner", "gender", "firstName","lastName","email","role", "menu", "terms","privacy","status","username","createdAt","updatedAt", "id","tokenId"]
	m_statusCode = 200
	m_request = SendGETRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")

	'''


	m_testName = "GET /usersettings/:parentId/fetchusersettings"
	print("")
	print("==============================================================================================")
	print("BEGINNING TEST FOR: " + m_testName)
	print("==============================================================================================")

	#To login the account with user role i.e., ‘PARENT’ or ‘TEACHER’. Requires ‘ACTIVE’ CMS records with user’s role in ‘cms’ collection.
	#REQ: Players resoonse body need to be expanded to test

	m_subTest = "HTTP Status Code: 200"
	print("Testing for " + m_subTest + "\n")
	m_testCount += 1

	m_baseURL = "https://services-qa.squarepanda.com"
	m_URLParameters = "/usersettings/"+m_accountInfov2['id']+"/fetchusersettings"
	m_tokenId = StringCombiner("Bearer",m_accountInfov2['tokenId'])
	print(m_tokenId+".")
	m_requestHeader = {"Authorization" : m_tokenId}
	m_requestBody = {}
	m_responseHeader = ""
	m_responseBody = ["owner", "gender", "firstName","lastName","email","role", "menu", "terms","privacy","status","username","createdAt","updatedAt", "id","tokenId"]
	m_statusCode = 200
	m_request = SendGETRequest(m_baseURL, m_URLParameters, m_requestHeader, m_requestBody)

	if(not RequestProcessing(m_request, m_responseHeader, m_responseBody, m_statusCode)):
		RecordError(m_testName, m_subTest, m_request, m_dictOfFailedTests)
	


	print("----------------------------------------------------------------------------------------------")






	print("----------------------------------------------------------------------------------------------")
	print("==============================================================================================")
	print("TEST SUMMARY")
	print("==============================================================================================")
	print("Total Tests: " + str(m_testCount) + "\n")
	print("Passed Tests: " + str(m_testCount - len(m_dictOfFailedTests)) + "\n")
	print("Failed Tests: " + str(len(m_dictOfFailedTests)) + "\n")

	PrintFail(m_dictOfFailedTests)
	print("==============================================================================================")


if __name__ == "__main__":
	execute()




'''
===============================================================================================================================================================================
Dictionary of error codes for test comparison
===============================================================================================================================================================================

'''


dict_of_error_codes = {
	
	"ERR1" : {"StatusCode" : "500" , "ResponseBody" : '{"err": "Token already Expired"}' , "description" : "Refresh token is not in 'tokens' collection."},
	"ERR2" : {"StatusCode" : "500" , "ResponseBody" : '{"err": "Token not Found"}' , "description" : "Unable to destroy existing token from ‘tokens’ collection."},
	"ERR3" : {"StatusCode" : "500" , "ResponseBody" : '{"message":"token required"}' , "description" : "Request body doesn’t have ‘refreshToken’ value."},
	"ERR4" : {"StatusCode" : "200" , "ResponseBody" : '{"message": "error"}' , "description" : "Unable to get or set records from database collection."},
	"ERR5" : {"StatusCode" : "200" , "ResponseBody" : '{"message:"activation_link_expired"}' , "description" : "Confirmation token is not in ‘user’ collection."},
	"ERR6" : {"StatusCode" : "200" , "ResponseBody" : '{"message": "error_expired"}' , "description" : "Confirmation token expired."},
	"ERR7" : {"StatusCode" : "500" , "ResponseBody" : '{"error":"Data error"}' , "description" : "Unable to get or set records from database collection."},
	"ERR8" : {"StatusCode" : "200" , "ResponseBody" : '{"message": "user already verified"}' , "description" : "User status is not ‘NOTVERIFIED’"},
	"ERR9" : {"StatusCode" : "500" , "ResponseBody" : '{"error":"user not found"}' , "description" : "User not in ‘user’ collection."},
	"ERR10" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.username.required"}' , "description" : "Request body doesn’t have ‘identifier’ value."},
	"ERR11" : {"StatusCode" : "409" , "ResponseBody" : '{"error": "error.login.user.Invalid"}' , "description" : "Invalid email"},
	"ERR12" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.password.required"}' , "description" : "Request body doesn’t have ‘password’ or ‘oldPassword’ values."},
	"ERR13" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.password.small"}' , "description" : "Password length is less than 8 characters."},
	"ERR14" : {"StatusCode" : "423" , "ResponseBody" : '{"error”: "error.user.loginAttempts.exceeded"}' , "description" : "User status is ‘INACTIVE’ or ‘DELETED’"},
	"ERR15" : {"StatusCode" : "424" , "ResponseBody" : '{"error":"error.user.email.NotVerified"}' , "description" : "User status is ‘NOTVERIFIED’"},
	"ERR16" : {"StatusCode" : "401" , "ResponseBody" : '{"error":"error.username.password.mismatch"}' , "description" : "Incorrect password."},
	"ERR17" : {"StatusCode" : "423" , "ResponseBody" : '{"error": "error.user.email.notFound"}' , "description" : "User email is not in ‘user’ collection."},
	"ERR18" : {"StatusCode" : "400" , "ResponseBody" : '{"error":" error.user.firstName.required"}' , "description" : "Request body doesn’t have ‘firstName’ value."},
	"ERR19" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.email.required"}' , "description" : "Request body doesn’t have ‘email’ value."},
	"ERR20" : {"StatusCode" : "500" , "ResponseBody" : '{"error": "error.user.email.failedtosend"}' , "description" : "Invalid email"},
	"ERR21" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.role.required"}' , "description" : "Request body doesn’t have ‘role’ value."},
	"ERR22" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.date.Invalid"}' , "description" : "Invalid date of birth"},
	"ERR23" : {"StatusCode" : "500" , "ResponseBody" : '{"error”: "error.user.role.notFound"}' , "description" : "Role is not in ‘role’ collection"},
	"ERR24" : {"StatusCode" : "400" , "ResponseBody" : '{"error”: "error.user.role.invalid"}' , "description" : "Invalid role"},
	"ERR25" : {"StatusCode" : "412" , "ResponseBody" : '{"message": "Email address is already taken"}' , "description" : "Active user exists with given email."},
	"ERR26" : {"StatusCode" : "405" , "ResponseBody" : '{"message": "Email already Activated"}' , "description" : "User status is not ‘NOTVERIFIED’"},
	"ERR27" : {"StatusCode" : "415" , "ResponseBody" : '{"error": "Email not available"}' , "description" : "User email is not in ‘user’ collection or user status is ‘NOTVERIFIED’."},
	"ERR28" : {"StatusCode" : "424" , "ResponseBody" : '{"error": "Email not verified. Please verify it"}' , "description" : "User status is not ‘NOTVERIFIED’ and ‘ACTIVE’"},
	"ERR29" : {"StatusCode" : "406" , "ResponseBody" : '{"message": "Email Not Acceptable"}' , "description" : "User status is ‘ACTIVE’ and not ‘NOTVERIFIED’"},
	"ERR30" : {"StatusCode" : "500" , "ResponseBody" : '{"error": "Your email status not in active. Please contact admin"}' , "description" : "User status is not ‘ACTIVE’"},
	"ERR31" : {"StatusCode" : "400" , "ResponseBody" : '{"error": “error.user.password.mismatch"}' , "description" : "Confirm password doesn’t match with password."},
	"ERR32" : {"StatusCode" : "500" , "ResponseBody" : '{"error": “error.user.token.expired"}' , "description" : "Reset password token expired."},
	"ERR33" : {"StatusCode" : "400" , "ResponseBody" : '{"error": “error.user.oldPassword.required"}' , "description" : "Request body doesn’t have ‘oldPassword’ value."},
	"ERR34" : {"StatusCode" : "500" , "ResponseBody" : '{"error": “error.user.oldPassword.wrong"}' , "description" : "Incorrect old password."},
	"ERR35" : {"StatusCode" : "500" , "ResponseBody" : '{"error": “error.user.userId.notExists"}' , "description" : "User not in ‘user’ collection."},
	"ERR36" : {"StatusCode" : "405" , "ResponseBody" : '{"error":  "Method Not Allowed"}' , "description" : "‘email’ and ‘role’ in request body are not allowed."},
	"ERR37" : {"StatusCode" : "200" , "ResponseBody" : '{"message": "Password reset token is invalid or has expired."}' , "description" : "Invalid or expired reset password token."},
	"ERR38" : {"StatusCode" : "200" , "ResponseBody" : '{"message": "User already in ANONYMOUS"}' , "description" : "User status is ‘ANONYMOUS’"},
	"ERR39" : {"StatusCode" : "200" , "ResponseBody" : '{"message": "User already in ACTIVE"}' , "description" : "User status is ‘ACTIVE’"},
	"ERR40" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "require child id\'s"}' , "description" : "Request body doesn’t have ‘childIds’ value."},
	"ERR41" : {"StatusCode" : "500" , "ResponseBody" : '{"error": "error.user.childId.notExists"}' , "description" : "Child id is not in ‘student’ collection."},
	"ERR42" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "User not available"}' , "description" : "User not in ‘user’ collection."},
	"ERR43" : {"StatusCode" : "412" , "ResponseBody" : '{"message": "Email already exists"}' , "description" : "Email exist in ‘user’ collection"},
	"ERR44" : {"StatusCode" : "500" , "ResponseBody" : '{"error":"error.changePassword.oldPassword.mismatch"}' , "description" : "Invalid old password."},
	"ERR45" : {"StatusCode" : "500" , "ResponseBody" : '{"error": "error.changePassword.user.notFound"}' , "description" : "User not in ‘user’ collection."},
	"ERR46" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "Menu name required"}' , "description" : "Request body doesn’t have ‘menuName’ or ‘subMenuName’ value."},
	"ERR47" : {"StatusCode" : "415" , "ResponseBody" : '{"error": "Unsupported Media Type"}' , "description" : "File(s) upload failed."},
	"ERR48" : {"StatusCode" : "413" , "ResponseBody" : '{"error": "Files size exceeds 10MB"}' , "description" : "Total size of all files exceeds 10MB"},
	"ERR49" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "Select one file"}' , "description" : "More than one file selected or no file selected."},
	"ERR50" : {"StatusCode" : "415" , "ResponseBody" : '{"error": "Improper Image Format"}' , "description" : "File(s) extension is not valid. Accepted formats are \"jpg\", \"jpeg\" and \"png\"."},
	"ERR51" : {"StatusCode" : "400" , "ResponseBody" : '{"message": "Files selected should be in the range of 1 to 5"}' , "description" : "Number of files selected should be between 1 to 5."},
	"ERR52" : {"StatusCode" : "409" , "ResponseBody" : '{"error": {"message": "Improper Image Format","filename": ["Screen-11 copy 2.jpg"]}"}' , "description" : "Error occured at image scale or orientation."},
	"ERR53" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "File data required"}' , "description" : "Request body doesn’t have ‘contents’ value."},
	"ERR54" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "something went wrong"}' , "description" : "Decode error or bucket upload error."},
	"ERR55" : {"StatusCode" : "400" , "ResponseBody" : '{"error": \'Invalid input string\'"}' , "description" : "Improper base64 format. Regex: /^data:([A-Za-z-+\/]+);base64,(.+)$/"},
	"ERR56" : {"StatusCode" : "400" , "ResponseBody" : '{"error": \'Invalid file type\'"}' , "description" : "Invalid file extension. Accepted values: ['image/jpeg', 'image/png', 'image/jpg']"},
	"ERR57" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.file.url.invalid"}' , "description" : "Invalid ‘profileURL’ or ’image_url’ value. Doesn’t match with cloud bucket url."},
	"ERR58" : {"StatusCode" : "406" , "ResponseBody" : '{"error": "Player Limit exceeded"}' , "description" : "User’s player limit exceeded."},
	"ERR59" : {"StatusCode" : "409" , "ResponseBody" : '{"message": "Student already exists"}' , "description" : "Player already exists with same ‘firstName’, ‘lastName’ and ‘teacherId’ in ‘student’ collection."},
	"ERR60" : {"StatusCode" : "409" , "ResponseBody" : '{"error": "Improper input json"}' , "description" : "Request body is not an array or empty array."},
	"ERR61" : {"StatusCode" : "405" , "ResponseBody" : '{"error": "Student already deleted."}' , "description" : "Child id is not in ‘student’ collection."},
	"ERR62" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "Unsupported User Role"}' , "description" : "User role is not ‘TEACHER’"},
	"ERR63" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "Unsupported File"}' , "description" : "Invalid data. Error occurred when converting XLS or CSV data to JSON."},
	"ERR64" : {"StatusCode" : "412" , "ResponseBody" : '{"message":"No title row"}' , "description" : "Selected MS-Excel file doesn’t have expected title row."},
	"ERR65" : {"StatusCode" : "204" , "ResponseBody" : '{"message":"No data to upload"}' , "description" : "Empty file or empty in corresponding title row values or no valid values."},
	"ERR66" : {"StatusCode" : "500" , "ResponseBody" : '{"error": "Request body is empty!"}' , "description" : "Request body doesn’t have ‘results’ value."},
	"ERR67" : {"StatusCode" : "500" , "ResponseBody" : '{"error": "error in update"}' , "description" : "Invalid feed id or firmware id."},
	"ERR68" : {"StatusCode" : "500" , "ResponseBody" : '{"error": "error.user.wordName.required"}' , "description" : "Request body doesn’t have ‘wordName’ value."},
	"ERR69" : {"StatusCode" : "500" , "ResponseBody" : '{"error": "Private words limit exceeded"}' , "description" : "User’s word count exceeds 50."},
	"ERR70" : {"StatusCode" : "500" , "ResponseBody" : '{"error": "Private word already exists"}' , "description" : "Word already exists in ‘privatewords’ collection with same user id."},
	"ERR71" : {"StatusCode" : "409" , "ResponseBody" : '{"error": "Improper value"}' , "description" : "‘imageUrl’ is not an array."},
	"ERR72" : {"StatusCode" : "400" , "ResponseBody" : '{"message": "Files selected should be in the range of 1 to 5"}' , "description" : "The max length of ‘imageUrl’ array exceeds 5."},
	"ERR73" : {"StatusCode" : "500" , "ResponseBody" : '{"error": "error.user.childId.notExists"}' , "description" : "Invalid word id."},
	"ERR74" : {"StatusCode" : "400" , "ResponseBody" : '{"error":"Firmware Revision String Required"}' , "description" : "Request body doesn’t have ‘firmware_revision_string’ value."},
	"ERR75" : {"StatusCode" : "409" , "ResponseBody" : '{"error":"Firmware version already exists"}' , "description" : "Firmware with same name and version already exists in ‘firmware’ collection with different firmware id."},
	"ERR76" : {"StatusCode" : "409" , "ResponseBody" : '{"error": "Irrelevant data"}' , "description" : "Request words doesn’t exist in ‘word’ collection."},
	"ERR77" : {"StatusCode" : "500" , "ResponseBody" : '{"error": "error in destory"}' , "description" : "Invalid firmware id."},
	"ERR78" : {"StatusCode" : "500" , "ResponseBody" : '{"error": "error in delete"}' , "description" : "Firmware is not allowed to remove."},
	"ERR79" : {"StatusCode" : "409" , "ResponseBody" : '{"error":"Firmware revision String Already Exists"}' , "description" : "Firmware with same name and version already exists in ‘firmware’ collection with different firmware id."},
	"ERR80" : {"StatusCode" : "409" , "ResponseBody" : '{"error":"Can\'t Edit"}' , "description" : "Firmware status is not ‘INACTIVE’"},
	"ERR81" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "Id mismatch"}' , "description" : "User id in URL parameters doesn’t match with the request body owner value."},
	"ERR82" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.req.body.invalid"}' , "description" : "Missing required fields in request body."},
	"ERR83" : {"StatusCode" : "400" , "ResponseBody" : '{"error":"Game name Required"}' , "description" : "Request body doesn’t have ‘gameName’ value."},
	"ERR84" : {"StatusCode" : "400" , "ResponseBody" : '{"error":"Game version Required"}' , "description" : "Request body doesn’t have ‘gameVersion’ value."},
	"ERR85" : {"StatusCode" : "409" , "ResponseBody" : '{"error":"Game Name Already Exists"}' , "description" : "Game name already exists in ‘game’ collection"},
	"ERR86" : {"StatusCode" : "400" , "ResponseBody" : '{"error":"Version Required"}' , "description" : "Request body doesn’t have ‘version’ value."},
	"ERR87" : {"StatusCode" : "400" , "ResponseBody" : '{"error":"Role Required"}' , "description" : "Request body doesn’t have ‘role’ value."},
	"ERR88" : {"StatusCode" : "409" , "ResponseBody" : '{"error":"Version Already Exists"}' , "description" : "CMS with same version, role and category already exists in ‘cms’ collection."},
	"ERR89" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "Req Body should not be empty"}' , "description" : "Empty request body."},
	"ERR90" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.userId.required"}' , "description" : "User id is invalid"},
	"ERR91" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.parentId.required"}' , "description" : "Admin Id is invalid"},
	"ERR92" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.childId.required"}' , "description" : "Student Id / Child Id invalid."},
	"ERR93" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.title.required"}' , "description" : "Request body doesn’t have ‘title’ value."},
	"ERR94" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.schoolName.required"}' , "description" : "Request body doesn’t have ‘schoolName’ value."},
	"ERR95" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.schoolDistrict.required"}' , "description" : "Request body doesn’t have ‘schoolDistrict’ value."},
	"ERR96" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.state.required"}' , "description" : "Request body doesn’t have ‘state’ value."},
	"ERR97" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.gradeLevels.required"}' , "description" : "Request body doesn’t have ‘gradeLevels’ value."},
	"ERR98" : {"StatusCode" : "400" , "ResponseBody" : '{"error": "error.user.gender.required"}' , "description" : "Request body doesn’t have ‘gender’ value."}

}