# SP Back-End API Tester

### Requirements:

- requests Library (http://docs.python-requests.org/en/master/)
- Python 3.6

### To Run:
In your preferred terminal, while in the root folder, run

	python3 SP_TestScript.py
